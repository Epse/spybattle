<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Challenge extends Model
{
    public static function allOpen() {
        return Challenge::where('open', true);
    }

    protected $fillable = [
        'title', 'open', 'reward', 'answer', 'solve_limit', 'description', 'score_reward'
    ];

    public function solves() {
        return $this->hasMany('App\Solve');
    }

    public function solvedBy(User $user) {
        return $this->solves()->where('user_id', $user->id)->exists();
    }

    public function canSolve(User $user) {
        if ($this->solve_limit > 0)
            return $this->solves()->count() < $this->solve_limit
                && !($this->solvedBy($user));
        else
            return !($this->solvedBy($user));
    }

    protected $casts = [
    ];

    protected $hidden = [
        'answer'
    ];
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class MakeAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mkuser:admin {username} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new admin.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('username');
        $email = $this->argument('email');
        $password = $this->ask('What is the password for the new user?');
        $user = new \App\User();
        $user->password = Hash::make($password);
        $user->email = $email;
        $user->name = $name;
        $user->is_admin = true;
        $user->save();

        $this->info('Successfully created the user. You can now log in.');
    }
}

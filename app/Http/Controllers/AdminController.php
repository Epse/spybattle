<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Challenge;
use App\User;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index() {
        $challenges = Challenge::all();
        $users = \App\User::allNonAdmin()->get();
        return view('admin', ['challenges' => $challenges, 'users' => $users]);
    }
}

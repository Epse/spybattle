<?php

namespace App\Http\Controllers;

use App\Challenge;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\ChallengeCreateRequest;
use App\Http\Requests\ChallengeEditRequest;
use App\Http\Requests\VerifyAnswerRequest;

class ChallengeController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin')->except('verify');
    }

    public function verify(VerifyAnswerRequest $request, Challenge $challenge) {
        if (!($challenge->open))
            abort(403);

        $answer = $request->validated()['answer'];
        $user = \Auth::user();
        if ($answer === $challenge->answer) {

            if (!($challenge->canSolve($user)))
                return redirect()->route('home', ['late' => $challenge->title]);

            $user->money += $challenge->reward;
            $user->score += $challenge->score_reward;
            $user->save();
            $solve = new \App\Solve();
            $solve->challenge()->associate($challenge);
            $user->solves()->save($solve);
            $challenge->save();
            return redirect()->route('home', ['solved' => $challenge->title]);
        }
        $user->money -= $challenge->penalty;
        $user->save();
        return redirect()->route('home', ['wrong' => $challenge->title]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChallengeCreateRequest $request)
    {
        $input = $request->validated();
        // remove null values
        $input = array_filter($input, function ($value) {
            return !is_null($value);
        });

        $challenge = new Challenge($input);
        $challenge->save();
        return redirect()->route('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Challenge  $challenge
     * @return \Illuminate\Http\Response
     */
    public function show(Challenge $challenge)
    {
        return view('challenge')->with('challenge', $challenge);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Challenge  $challenge
     * @return \Illuminate\Http\Response
     */
    public function edit(Challenge $challenge)
    {
        return view('edit')->with('challenge', $challenge);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Challenge  $challenge
     * @return \Illuminate\Http\Response
     */
    public function update(ChallengeEditRequest $request, Challenge $challenge)
    {
        $input = $request->validated();
        // remove null values
        $input = array_filter($input, function ($value) {
            return !is_null($value);
        });

        $challenge->update($input);
        $challenge->save();
        return redirect()->route('admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Challenge  $challenge
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChallengeEditRequest $request, Challenge $challenge)
    {
        $challenge->delete();
        return redirect()->route('admin');
    }
}

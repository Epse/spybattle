<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Challenge;
use App\Solve;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $challenges = Challenge::allOpen()->get();
        try {
            $prev = Solve::orderBy('created_at', 'desc')->first()->id;
        } catch (\ErrorException $e) {
            $prev = -1;
        }
        if ($request->has('solved'))
            return view('home', ['prev' => $prev, 'challenges' => $challenges, 'solved' => $request->solved]);
        if ($request->has('wrong'))
            return view('home', ['prev' => $prev, 'challenges' => $challenges, 'wrong' => $request->wrong]);
        if ($request->has('late'))
            return view('home', ['prev' => $prev, 'challenges' => $challenges, 'late' => $request->late]);
        return view('home', ['prev' => $prev, 'challenges' => $challenges]);
    }
}

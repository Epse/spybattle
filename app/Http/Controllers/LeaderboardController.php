<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LeaderboardController extends Controller
{
    public function index() {
        $teams = \App\User::allNonAdmin()->with('solves')->get();
        $teams->sortBy(function($team) {
            return $team->solves->count();
        });

        return view('leaderboard')->with('teams', $teams);
    }

    public function data() {
        $teams = \App\User::allNonAdmin()->orderBy('score', 'desc')->get();
        return $teams;
    }
}

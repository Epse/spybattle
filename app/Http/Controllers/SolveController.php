<?php

namespace App\Http\Controllers;

use App\Solve;
use Illuminate\Http\Request;

class SolveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('start') && $request-> start > 0) {
            return Solve::where('id', '>', $request->start)->with(['user', 'challenge'])->get();
        }
        return Solve::with(['user', 'challenge'])->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Solve  $solve
     * @return \Illuminate\Http\Response
     */
    public function show(Solve $solve)
    {
        //
    }
}

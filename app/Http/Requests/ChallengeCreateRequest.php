<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class ChallengeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|filled|max:200|string',
            'description' => 'required|filled|string',
            'answer' => 'required|filled|max:200|string',
            'reward' => 'required|filled|integer',
            'open' => 'nullable|boolean',
            'solve_limit' => 'nullable|filled|integer|min:0',
            'score_reward' => 'required|filled|integer',
            'penalty' => 'required|filled|integer|min:0'
        ];
    }
}

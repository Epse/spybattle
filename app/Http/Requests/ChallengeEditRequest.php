<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class ChallengeEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'nullable|filled|max:200|string',
            'description' => 'nullable|filled|string',
            'answer' => 'nullable|filled|max:200|string',
            'reward' => 'nullable|filled|integer',
            'open' => 'nullable|boolean',
            'solve_limit' => 'nullable|integer|min:0',
            'score_reward' => 'nullable|filled|integer',
            'penalty' => 'nullable|filled|integer|min:0'
        ];
    }
}

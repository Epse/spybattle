<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solve extends Model
{
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function challenge() {
        return $this->belongsTo('App\Challenge');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallenges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenges', function (Blueprint $table) {
            $table->string('title', 200);
            $table->boolean('solved')->default(false);
            $table->boolean('open')->default(false);
            $table->integer('reward')->unsigned()->default(50);
            $table->string('answer', 200);
            $table->text('description');
            $table->boolean('exclusive')->default(false);
            $table->bigIncrements('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenges');
    }
}

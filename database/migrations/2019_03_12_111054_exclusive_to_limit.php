<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExclusiveToLimit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('challenges', function (Blueprint $table) {
            $table->dropColumn('solved');
            $table->integer('solve_limit')->unsigned()->default(0);
        });
        DB::table('challenges')->where('exclusive', true)->update(['solve_limit' => 1]);
        Schema::table('challenges', function (Blueprint $table) {
            $table->dropColumn('exclusive');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('challenges', function (Blueprint $table) {
            $table->boolean('solved')->default(false);
            $table->boolean('exclusive')->default(false);
        });
        DB::table('challenges')->where('solve_limit', '>', 0)->update(['exclusive' => true]);
        Schema::table('challenges', function (Blueprint $table) {
            $table->dropColumn('solve_limit');
        });
    }
}

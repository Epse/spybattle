@extends('layouts.app')

@section('title', '- Admin')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Admin</div>
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <h1>Admin</h1>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h5>
                            <a class="d-block" href="#nwuitdaging" data-toggle="collapse">Nieuwe uitdaging</a>
                        </h5>
                    </div>
                    <div class="card-body collapse" id="nwuitdaging">
                        @if($errors->any())
                            <div class="alert alert-warning">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ route('challenges.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="title">Titel</label>
                                <input name="title" id="title" type="text" value="{{ old('title') }}" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="description">Beschrijving</label>
                                <textarea class="form-control" id="description" name="description" type="text" value="{{ old('description') }}"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="answer">Antwoord</label>
                                <input id="answer" class="form-control" name="answer" type="text" value="{{ old('answer') }}"/>
                            </div>
                            <div class="form-group">
                                <label for="reward">Geldbeloning</label>
                                <input id="reward" class="form-control" name="reward" type="number" value="{{ old('reward') }}"/>
                            </div>
                            <div class="form-group">
                                <div class="label" for="score_reward">
                                    Beloning
                                </div>
                                <input class="form-control" id="score_reward" name="score_reward" type="number" value="{{ old('score_reward') }}"/>
                            </div>
                            <div class="form-group">
                                <label for="solve_limit">Limiet (0 voor geen limiet)</label>
                                <input id="solve_limit" class="form-control" name="solve_limit" type="number" min="0" value="{{ old('solve_limit', 0) }}" />
                            </div>
                            <div class="form-group form-check">
                                <input id="open" class="form-check-input" name="open" type="checkbox" value="1" />
                                <label for="open">Open</label>
                            </div>
                            <button type="submit" class="btn btn-primary">Opslaan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <h2>Teams</h2>
        @foreach ($users as $user)
            <div class="row justify-content-center mb-3">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5>
                                <a class="d-block" href="#user{{ $user->id }}" data-toggle="collapse">{{ $user->name }}</a>
                            </h5>
                        </div>
                        <div class="card-body collapse" id="user{{ $user->id }}">
                            <p class="card-text">
                                <a href="{{ route('users.show', ['user' => $user]) }}">{{ $user->name }}</a>
                            </p>
                            <hr/>
                            <p class="card-text">
                                Bankrekening: €{{ $user->money }}
                            </p>
                            <p class="card-text">
                                {{ $user->solves()->count() }} challenge(s) opgelost.
                            </p>
                            <hr/>
                            <form action="{{ route('users.pay', ['user' => $user]) }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="amount">Te betalen</label>
                                    <input class="form-control" name="amount" type="number" value="{{ old('amount') }}"/>
                                </div>
                                <button type="submit" class="btn btn-primary">Betalen</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <h2>Challenges</h2>
        @foreach ($challenges as $challenge)
            @component('components.adminchallenge', ['challenge' => $challenge])
            @endcomponent
        @endforeach
    </div>
@endsection

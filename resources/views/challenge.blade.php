@extends('layouts.app')

@section('title', '- ' . $challenge->title)

@section('content')
    <div class="container">
        <h1>
            {{ $challenge->title }}
            @if ($challenge->solve_limit != 0)
                <span class="float-right badge badge-info mx-1">
                    Limiet: {{ $challenge->solve_limit }}
                </span>
            @else
                <span class="float-right badge badge-success mx-1">
                    Opgelost: {{ $challenge->solves->count() }}
                </span>
            @endif
            @if ($challenge->open)
                <span class="float-right badge badge-primary mx-1">
                    Open
                </span>
            @endif
        </h1>

        <p>
            @markdown($challenge->description)
        </p>

        <hr/>
        <h2>Eigenschappen</h2>
        @can('admin')
            <p class="card-text">
                <strong>Antwoord: </strong>{{ $challenge->answer }}
            </p>
        @endcan
        <p class="card-text">
            <strong>Geldbeloning: </strong>€{{ $challenge->reward }}
        </p>
        <p class="card-text">
            <strong>Beloning: </strong>{{ $challenge->score_reward }} punten
        </p>
        <p class="card-text">
            <strong>Straf voor fout: </strong>€{{ $challenge->penalty }}
        </p>

        <hr/>
        <h2>Opgelost door</h2>
        <ul>
            @foreach ($challenge->solves as $solve)
                <li><a href="{{ route('users.show', ['user' => $solve->user]) }}">{{ $solve->user->name }}</a></li>
            @endforeach
        </ul>

    </div>
@endsection

<div class="row justify-content-center mb-3">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5>
                    <a class="d-block" href="#challenge{{ $challenge->id }}" data-toggle="collapse">
                        {{ $challenge->title }}
                    </a>
                </h5>
            </div>
            <div class="collapse card-body" id="challenge{{ $challenge->id }}">
                <p class="card-text">
                    <h5 class="card-title">
                        <a href="{{ route('challenges.show', ['challenge' => $challenge]) }}">{{ $challenge->title }}</a>
                    </h5>
                    @markdown($challenge->description)
                </p>
                <hr/>
                <p class="card-text">
                    <strong>Antwoord: </strong>{{ $challenge->answer }}
                </p>
                <p class="card-text">
                    <strong>Geldbeloning: </strong>€{{ $challenge->reward }}
                </p>
                <p class="card-text">
                    <strong>Straf voor fout: </strong>€{{ $challenge->penalty }}
                </p>
                <p class="card-text">
                    <strong>Beloning: </strong>{{ $challenge->score_reward }} punten
                </p>
                @if ($challenge->solve_limit == 0)
                    <p class="card-text">
                        <strong>Limiet:</strong> geen
                    </p>
                @else
                    <p class="card-text">
                        <strong>Limiet:</strong> {{ $challenge->solve_limit }} oplossingen
                    </p>
                @endif

                <hr />

                <p class="card-text">
                    {{ $challenge->solves->count() }} keer opgelost.
                </p>

                <form action="{{ route('challenges.update', ['challenge' => $challenge]) }}" method="post">
                    @csrf
                    @method('PATCH')
                    @if (!($challenge->open))
                        <input name="open" type="hidden" value="1"/>
                        <button type="submit" class="btn btn-primary">Open</button>
                    @else
                        <input name="open" type="hidden" value="0"/>
                        <button type="submit" class="btn btn-secondary">Sluit</button>
                    @endif
                    <a class="btn btn-secondary" href="{{ route('challenges.edit', ['challenge' => $challenge]) }}">Bewerk</a>
                </form>
                {{--
                    <form action="{{ route('challenges.destroy', ['challenge' => $challenge]) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger mt-1" type="submit">Verwijder</button>
                    </form> --}}
            </div>
        </div>
    </div>
</div>

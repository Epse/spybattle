<div class="row justify-content-center mb-3">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header clearfix">
                <h5>
                    <a href="#challenge{{ $challenge->id }}" class="d-block" data-toggle="collapse">
                        {{ $challenge->title }}
                        @if ($challenge->solve_limit != 0)
                            <span class="float-right badge badge-info mx-1">
                                Limiet: {{ $challenge->solve_limit }}
                            </span>
                        @endif
                        @if ($challenge->solvedBy(Auth::user()))
                            <span class="float-right badge badge-success mx-1">
                                Opgelost (jou)
                            </span>
                        @elseif (!($challenge->canSolve(Auth::user())))
                            <span class="float-right badge badge-secondary mx-1">
                                Opgelost
                            </span>
                        @endif
                    </a>
                </h5>
            </div>
            <div class="collapse card-body" id="challenge{{ $challenge->id }}">
                <p class="card-text">
                    @markdown($challenge->description)
                </p>
                <hr/>
                <p class="card-text">
                    <strong>Geldbeloning: </strong>€{{ $challenge->reward }}
                </p>
                <p class="card-text">
                    <strong>Straf voor fout: </strong>€{{ $challenge->penalty }}
                </p>
                <p class="card-text">
                    <strong>Beloning: </strong>{{ $challenge->score_reward }} punten
                </p>

                @if ($challenge->canSolve(Auth::user()))
                    <hr />
                    <form action="{{ route('challenges.verify', ['challenge' => $challenge]) }}" method="post">
						@csrf
                        <div class="form-group">
                            <label for="answer">Antwoord</label>
                            <input class="form-control" id="answer" name="answer" type="text" value="{{ old('answer') }}"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Verzend</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>

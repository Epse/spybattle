@extends('layouts.app')

@section('title', '- Bewerk ' . $challenge->title)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if($errors->any())
                    <div class="alert alert-warning">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="{{ route('challenges.update', ['challenge' => $challenge]) }}" method="post">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                        <label for="title">Titel</label>
                        <input name="title" id="title" type="text" value="{{ old('title', $challenge->title) }}" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="description">Beschrijving</label>
                        <textarea class="form-control" id="description" name="description" type="text">{{ old('description', $challenge->description) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="answer">Antwoord</label>
                        <input id="answer" class="form-control" name="answer" type="text" value="{{ old('answer', $challenge->answer) }}"/>
                    </div>
                    <div class="form-group">
                        <label for="reward">Geldbeloning</label>
                        <input id="reward" class="form-control" name="reward" type="number" value="{{ old('reward', $challenge->reward) }}"/>
                    </div>
                    <div class="form-group">
                        <div class="label" for="score_reward">
                            Beloning
                        </div>
                        <input class="form-control" id="score_reward" name="score_reward" type="number" value="{{ old('score_reward', $challenge->score_reward) }}"/>
                    </div>
                    <div class="form-group">
                        <label for="solve_limit">Limiet (0 voor geen limiet)</label>
                        <input id="solve_limit" class="form-control" name="solve_limit" type="number" min="0" value="{{ old('solve_limit', $challenge->solve_limit) }}" />
                    </div>
                    <div class="form-group form-check">
                        <input name="open" type="hidden" value="0"/>
                        <input id="open" class="form-check-input" name="open" type="checkbox" value="1" @if(old('open', $challenge->open)) checked @endif />
                        <label for="open">Open</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Opslaan</button>
                </form>
            </div>
        </div>
    </div>
@endsection

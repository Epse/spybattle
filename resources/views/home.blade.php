@extends('layouts.app')
@section('title', '- Home')

@section('content')
    <div class="container">
        @if (isset($solved))
            <div class="row">
                <div class="col">
                    <div class="alert alert-success">
                        Je hebt de opdracht <span class="font-italic">{{ $solved }}</span> correct opgelost!
                    </div>
                </div>
            </div>
        @endif

        @if (isset($wrong))
            <div class="row">
                <div class="col">
                    <div class="alert alert-danger">
                        Je antwoord op de opdracht <span class="font-italic">{{ $wrong }}</span> was fout.
                        Je bent nu geld kwijt.
                    </div>
                </div>
            </div>
        @endif

        @if (isset($late))
            <div class="row">
                <div class="col">
                    <div class="alert alert-secondary">
                        Iemand anders heeft de opdracht <i>{{ $late }}</i> al opgelost.
                    </div>
                </div>
            </div>
        @endif

        <!-- Wrapper for toasts -->
        <div id="toastapp">
            <div class="alert alert-warning" v-for="solve in solves" role="alert">
                <h4 class="alert-heading">
                    Uitdaging opgelost
                </h4>
                <p>
                    De uitdaging <i>@{{ solve.challenge.title }}</i> is opgelost door team <i>@{{ solve.user.name }}</i>.
                </p>
            </div>
        </div>

        @if (session('status'))
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <h5 class="card-header">Dashboard</h5>
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row justify-content-center mb-3">
            <div class="col-md-8">
                <div class="card">
                    <h5 class="card-header">
                        {{ Auth::user()->name }}
                    </h5>
                    <div class="card-body">
                        <p class="card-text">
                            €{{ Auth::user()->money }}
                        </p>
                        <p class="card-text">
                            {{ Auth::user()->score }} punten
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <h1>Challenges</h1>

        @if ($challenges->count() === 0)
            <div class="alert alert-primary">
                Geen uitdagingen beschikbaar.
            </div>
        @endif

        @if($errors->any())
            <div class="alert alert-warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @foreach ($challenges as $challenge)
            @component('components.submitchallenge', ['challenge' => $challenge])
            @endcomponent
        @endforeach
    </div>
@endsection

@section('js')
    <script>
        var app = new Vue({
            el: '#toastapp',
            data: {
                solves: [],
                prev: {{ $prev }}
            },
            methods: {
                loadData: function() {
                    fetch('{{ route('solves') }}?start=' + this.prev)
                        .then(res => res.json())
                        .then(function(out) {
                            this.solves = out;
                            var i = out.length - 1;
                            if (i >= 0)
                                this.prev = out[i].id;
                        }.bind(this));
                },
            },
            mounted: function() {
                this.loadData();

                setInterval(function() {
                    this.loadData();
                }.bind(this), 10000);
            }
        });
    </script>
@endsection

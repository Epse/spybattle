@extends('layouts.app')

@section('title', '- Leaderboard')

@section('extrascript')
    <script src="{{ asset('js/chart.min.js') }}"></script>
@endsection

@section('content')
    <style>
        @media (min-width: 992px)
        {
            #leaderboardcontainer {
                min-width: 90%;
                width: 90%;
            }
        }
    </style>
    </style>
    <div id="leaderboardcontainer" class="container">
        <div class="row">
            <div class="col">
                <canvas id="scoreCanvas"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="table-responsive" id="vue-table">
                    <table class="table table-dark table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Naam</th>
                                <th scope="col">Score</th>
                                <th scope="col">Geld</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(team, index) in teams" :key="index">
                                <th scope="row">@{{ index+1 }}</th>
                                <td><a v-bind:href="teamUrl + '/' + team.id" class="text-light">@{{ team.name }}</a></td>
                                <td>@{{ team.score }} punten</td>
                                <td>€ @{{ team.money }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var barChartData = {
            labels: ['Laden'],
            datasets: [{
                label: 'Score',
                backgroundColor: "rgba(100, 0, 0, 0.5)",
                yAxisID: 'solves-y-axis',
                data: [1]
            }, {
                label: 'Geld',
                backgroundColor: "rgba(0, 0, 100, 0.5)",
                yAxisID: 'money-y-axis',
                data: [1]
            }]
        };

        var app = new Vue({
            el: '#vue-table',
            data: {
                teams: [
                    {
                        name: "Laden",
                        solves: [1, 2],
                        money: 10,
                    }
                ],
                teamUrl: "{{ route('users.show', ['user' => '']) }}"
            }
        });


        function updateData() {
            fetch("{{ route('leaderboard.data') }}")
                .then(res => res.json())
                .then((out) => {
                    app.teams = out;
                    barChartData.labels = out.map(x => x.name);
                    barChartData.datasets[0].data = out.map(x => x.score);
                    barChartData.datasets[1].data = out.map(x => x.money);
                    window.myBar.update();
                });
        }

        window.onload = function() {
			var ctx = document.getElementById('scoreCanvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Leaderboard'
					},
                    scales: {
                        yAxes: [{
                            id: 'solves-y-axis',
                            type: 'linear',
                            position: 'left',
                            ticks: {
                                beginAtZero: true,
                                min: 0,
                            },
                        },{
                            id: 'money-y-axis',
                            type: 'linear',
                            position: 'right',
                            ticks: {
                                beginAtZero: true,
                                min: 0,
                            },
                        }]
                    }
				}
			});
            updateData();
            window.setInterval(updateData, 5000);
		};
    </script>
@endsection

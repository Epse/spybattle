@extends('layouts.app')

@section('title', '- '.$user->name)

@section('content')
    <div class="container">
        <h1>
            {{ $user->name }}
            @if ($user->is_admin)
                <span class="float-right badge badge-danger mx-1">
                    Admin
                </span>
            @endif
        </h1>

        <hr/>
        <form action="{{ route('users.destroy', $user) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit">Delete User</button>
        </form>
        <a class="btn btn-danger" href="{{ route("delete-user", $user) }}">Delete user</a>
        <hr />
        <h2>Eigenschappen</h2>
        <p class="card-text">
            <strong>Email: </strong>{{ $user->email }}
        </p>
        <p class="card-text">
            <strong>Geld: </strong>€{{ $user->money }}
        </p>
        <p class="card-text">
            <strong>Score: </strong>{{ $user->score }} punten
        </p>

        <hr/>
        <h2>Opgeloste uitdagingen</h2>
        <ul>
            @foreach ($user->solves as $solve)
                <li><a href="{{ route('challenges.show', ['challenge' => $solve->challenge]) }}">{{ $solve->challenge->title }}</a></li>
            @endforeach
        </ul>

    </div>
@endsection

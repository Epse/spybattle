<?php

use Illuminate\Http\Request;
use App\Http\Controllers\LeaderboardController;
use App\Http\Controllers\SolveController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/leaderboard/data', [LeaderboardController::class, 'data'])->name('leaderboard.data');
Route::get('/solves', [SolveController::class, 'index'])->name('solves');

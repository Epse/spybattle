<?php

use \App\Http\Controllers\HomeController;
use \App\Http\Controllers\AdminController;
use \App\Http\Controllers\ChallengeController;
use \App\Http\Controllers\LeaderboardController;
use \App\Http\Controllers\UserController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::get('/admin', [AdminController::class, 'index'])->name('admin');

Route::resource('challenges', 'ChallengeController',
                [
                    'except' => ['index', 'create', 'destroy'],
                ]);
Route::post('/challenges/{challenge}/verify', [ChallengeController::class, 'verify'])
    ->name('challenges.verify');

Route::post('/users/{user}/pay', [UserController::class, 'pay'])->name('users.pay');
Route::get('/users/{user}', [UserController::class, 'show'])->name('users.show');
Route::delete('/users/{user}', [UserController::class, 'destroy'])->name('users.destroy');

Route::get('/leaderboard', [LeaderboardController::class, 'index'])->name('leaderboard');

Auth::routes();
